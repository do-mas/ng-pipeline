#!/bin/bash

set -e -u -x

cd source-code

npm install

ng build --prod


cp Staticfile dist
cp -r ./dist ../build-out/dist
cp -r ./manifest.yml ../build-out/manifest.yml
